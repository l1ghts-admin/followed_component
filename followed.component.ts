import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Follow } from '../../models/follow';
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';
import { GLOBAL } from '../../services/global';

@Component({
	selector: 'followed',
	templateUrl: './followed.component.html',
	providers: [UserService, FollowService]
})
export class FollowedComponent implements OnInit{
	public title: string;
	public url: string;
	public identity;
	public token;
	public page: number | undefined;
	public next_page: number | undefined;
	public prev_page: number | undefined;
	public total: any;
	public pages: number | undefined;
	public users: User[] | undefined;
	public follows: any[] | undefined;
	public followed: any;
	public status: string | undefined;
	public userPageId: any;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _followService: FollowService
	){
		this.title = 'Seguidores de';
		this.url = GLOBAL.url;
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
	}

	ngOnInit(){
		console.log("users.component ha sido cargado");
		this.actualPage();
	}

	actualPage(){
		this._route.params.subscribe(params => {
			let user_id = params['id'];
			this.userPageId = user_id;

			let page = +params['page'];
			this.page = page;

			if(!params['page']){
				page = 1;
			}

			if(!page){
				page = 1;
			}else{
				this.next_page = page+1;
				this.prev_page = page-1;

				if(this.prev_page <= 0){
					this.prev_page = 1;
				}
			}

			// devolver listado de usuarios
			this.getUser(user_id, page);
		});
	}

	getFollows(user_id: null | undefined, page: number | undefined){
		this._followService.getFollowed(this.token, user_id, page).subscribe(
      (			response: { follows: any; total: any; pages: number | undefined; users_following: any[] | undefined; }) => {
				if(!response.follows){
					this.status = 'error';
				}else{
					console.log(response);

					this.total = response.total;
					this.followed = response.follows;
					this.pages = response.pages;
					this.follows = response.users_following;

					if(page && this.pages && page > this.pages){
						this._router.navigate(['/gente',1]);
					}

				}
			},
      (error: any) => {
				var errorMessage = <any>error;
				console.log(errorMessage);

				if(errorMessage != null){
					this.status = 'error';
				}
			}
		);
	}

	public user: User | undefined;
	getUser(user_id: any, page: number | undefined){
		this._userService.getUser(user_id).subscribe(
			response => {
				if(response.user){
					this.user = response.user;
					this.getFollows(user_id,page);
				}else{
					this._router.navigate(['/home']);
				}
			},
			error => {
				var errorMessage = <any>error;
				console.log(errorMessage);

				if(errorMessage != null){
					this.status = 'error';
				}
			}
		);
	}

	public followUserOver: number | undefined;
	mouseEnter(user_id: number){
		this.followUserOver = user_id;
	}

	mouseLeave(){
		this.followUserOver = 0;
	}

	followUser(followed: String){
		var follow = new Follow('',this.identity._id, followed);

		this._followService.addFollow(this.token, follow).subscribe(
      (response: { follow: any; }) => {
				if(!response.follow){
					this.status = 'error';
				}else{
					this.status = 'success';
					this.follows.push(followed);
				}
			},
      (error: any) => {
				var errorMessage = <any>error;
				console.log(errorMessage);

				if(errorMessage != null){
					this.status = 'error';
				}
			}
		);
	}

	unfollowUser(followed: any){
    let follows = this.follows ? this.follows : null;
		this._followService.deleteFollow(this.token, followed).subscribe(
			() =>{
				var search = follows ? this.follows.indexOf(followed): 0;
				if(search != -1){
					this.follows.splice(search, 1);
				}
			},
      (error: any) => {
				var errorMessage = <any>error;
				console.log(errorMessage);

				if(errorMessage != null){
					this.status = 'error';
				}
			}
		);
	}
}
